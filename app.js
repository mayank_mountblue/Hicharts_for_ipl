var express = require("express");
var app = express();
var bp = require("body-parser");
var path = require("path");
var fs =require("fs");
var csvjson = require('csvjson');
var $ = require('jquery'); 
app.use(express.static("public"));
app.use(bp.urlencoded({ extended: false }))
 
var data = fs.readFileSync(path.join(__dirname, 'data/matches.csv'), { encoding : 'utf8'});
function matchStat(){

var matchPlayedPerYear ={};
var jsondata=  csvjson.toObject(data);

for (i = 0; i < jsondata.length; i++) {
 var season =parseInt(jsondata[i].season);
 

var total_match=matchPlayedPerYear[season];
if(total_match==""||total_match==null||total_match==undefined){
  matchPlayedPerYear[season]=1;
 
} 
else{
  matchPlayedPerYear[season]=matchPlayedPerYear[season]+1;
 
}}
var season =Object.keys(matchPlayedPerYear);
console.log(season);
var match_per_season =[];
for(match in matchPlayedPerYear){
  match_per_season.push(matchPlayedPerYear[match]);
}
var matchAndSeason =[];

  Object.keys(matchPlayedPerYear).forEach(function(key) {
    var arr=[];
    arr[0]=key;
  arr[1]=matchPlayedPerYear[key];
   matchAndSeason.push(arr);
  
  })
  return matchAndSeason;
}
app.get("/getGraphData",function(req,res){
  console.log("in /");
   var data =matchStat();
  res.send(data);

  }
 
  
);
app.listen(5090);